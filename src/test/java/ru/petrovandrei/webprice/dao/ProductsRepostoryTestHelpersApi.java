package ru.petrovandrei.webprice.dao;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;
import javax.inject.Inject;
import lombok.val;
import org.junit.Before;
import org.springframework.transaction.annotation.Transactional;
import ru.petrovandrei.webprice.domain.Category;
import ru.petrovandrei.webprice.domain.Product;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static ru.petrovandrei.util.ArrayUtil.arraysToHashSet;
import static ru.petrovandrei.util.ArrayUtil.hashSetOf;
import static ru.petrovandrei.util.NullEmptyCheckUtil.isNullOrEmpty;
import static ru.petrovandrei.webprice.dao.BigDecUtil.toBigDec;
import static ru.petrovandrei.webprice.domain.Category.newCategory;

public abstract class ProductsRepostoryTestHelpersApi {

    @Inject
    private ProductsRepostory productsRepostory;

    @Inject
    private CategoriesRepository categoriesRepository;

    @Before public void prepareStorage() {
        productsRepostory.deleteAll();
        categoriesRepository.deleteAll();
        inflate();
    }

    abstract void inflate();

    //<editor-fold defaultstate="collapsed" desc="add">
    Category add(String categoryName, Product... productsInThisCategory) {
        return add(categoryName, asList(productsInThisCategory));
    }

    @Transactional
    Category add(String categoryName, Iterable<Product> productsInThisCategory) {
        val category = categoriesRepository.save(newCategory(categoryName));
        for (val product : productsInThisCategory) {
            product.setCategory(category);
            productsRepostory.save(product);
        }
        categoriesRepository.flush();
        productsRepostory.flush();
        return category;
    }

    @Transactional
    List<Product> add(Iterable<Product> products) {
        val saved = productsRepostory.save(products);
        productsRepostory.flush();
        return saved;
    }

    List<Product> add(Stream<Product> products) {
        return add(() -> products.iterator());
    }
    //</editor-fold>

    Category[] categories;
    Number from, to;
    String text;
    List<Product> found;

    void cats(Category... cats) {
        this.categories = cats;
    }

    void search() {
        val noCategories = isNullOrEmpty(categories);
        found = productsRepostory.findBy(noCategories,
                noCategories
                        ? emptyList()
                        : stream(categories).map(c -> c.getId()).collect(toList()),
                text, toBigDec(from), toBigDec(to)
        );
    }

    //<editor-fold defaultstate="collapsed" desc="verify">
    void shouldBeFound_ignoreOrder(HashSet<Product> products) {
        assertEquals(new HashSet<>(found), products);
    }

    void shouldBeFound_ignoreOrder(Product[]... arraysOfProducts) {
        shouldBeFound_ignoreOrder(arraysToHashSet(
                arraysOfProducts
        ));
    }

    void shouldBeFound_ignoreOrder(Product... products) {
        shouldBeFound_ignoreOrder(hashSetOf(products));
    }

    void shouldBeFound_ignoreOrder(List<Product> products) {
        shouldBeFound_ignoreOrder(new HashSet<>(products));
    }

    void shouldBeFound_matchOrder(List<Product> procucts) {
        assertEquals(found, procucts);
    }
    //</editor-fold>

}