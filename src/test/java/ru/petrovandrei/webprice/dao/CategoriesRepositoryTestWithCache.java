package ru.petrovandrei.webprice.dao;

import javax.inject.Inject;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static ru.petrovandrei.webprice.domain.Category.newCategory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
    "classpath:spring-datasource-test-config.xml",
    "classpath:spring-persistence-config.xml",
    "classpath:spring-cache-config.xml"
})
public class CategoriesRepositoryTestWithCache {

    @Inject
    CategoriesRepository cr;

    @Test public void shouldBeSameReference() {
        val a = cr.categoryIdsAvailableForUser();
        val b = cr.categoryIdsAvailableForUser();
        assertTrue(a == b);
        cr.save(newCategory("doesn't matter"));
        val c = cr.categoryIdsAvailableForUser();
        val d = cr.categoryIdsAvailableForUser();
        assertTrue(b != c);
        assertTrue(c == d);
    }
}