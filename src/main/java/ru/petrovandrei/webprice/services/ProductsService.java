package ru.petrovandrei.webprice.services;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.petrovandrei.webprice.controllers.ProductsController.SearchRequest;
import ru.petrovandrei.webprice.dao.ProductsRepostory;
import ru.petrovandrei.webprice.domain.Product;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

@Service
public class ProductsService {

    @Inject
    ProductsRepostory productsRepostory;

    public Product findById(Integer id) {
        return productsRepostory.findOne(id);
    }

    public byte[] findImageById(Integer id) {
        return productsRepostory.findImageById(id);
    }

    public List<Product> findBy(SearchRequest req) {
        log.info("ProductsService#findBy: {}", req);
        val categoryIds = req.getC();
        return productsRepostory.findBy(
                categoryIds == null || categoryIds.isEmpty(),
                categoryIds,
                req.getText(),
                req.getCostFrom(),
                req.getCostTo()
        );
    }

    public void save(Product product, MultipartFile image) throws IOException {
        log.info("ProductsService#save {}", product);
        if (image != null) {
            product.setImage(image.getBytes());
        }
        Product saved = productsRepostory.saveAndFlush(product);
        log.info("ProductsService#save saved: {}", saved);
    }

    public void delete(int id) {
        log.info("ProductsService#delete id: {}", id);
        productsRepostory.delete(id);
        log.info("deleted id: {}", id);
    }
}