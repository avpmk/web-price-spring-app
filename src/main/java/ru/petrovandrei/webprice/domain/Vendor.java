package ru.petrovandrei.webprice.domain;

import javax.persistence.Entity;

@Entity
public class Vendor extends BaseEntity {

    @Override protected final boolean instanceOfProperClass(BaseEntity another) {
        return another instanceof Vendor;
    }
}