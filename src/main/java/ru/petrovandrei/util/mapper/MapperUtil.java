package ru.petrovandrei.util.mapper;

import java.beans.PropertyEditorSupport;
import java.util.function.Function;

public class MapperUtil {

    public static <T> PropertyEditorSupport mapper(
            Function<String, T> fromString,
            Function<T, String> toString
    ) {
        return new PropertyEditorSupport() {

            @Override public void setAsText(String text) {
                setValue(fromString.apply(text));
            }

            @Override public String getAsText() {
                return toString.apply((T) getValue());
            }
        };
    }
}