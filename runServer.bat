@echo off
cls

if not defined JAVA_HOME (
    echo Gradle needs JAVA_HOME system variable to comlile java files.
) else (
    pushd %~dp0

    for %%X in (gradle) do set aPathToInstalledGradle=%%~$PATH:X

    if defined aPathToInstalledGradle (
        gradle r
    ) else (
        gradlew r
    )
)
pause