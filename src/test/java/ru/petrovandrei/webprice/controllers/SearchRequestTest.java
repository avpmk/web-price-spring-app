package ru.petrovandrei.webprice.controllers;

import java.math.BigDecimal;
import org.junit.Test;
import ru.petrovandrei.webprice.controllers.ProductsController.SearchRequest;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.*;

public class SearchRequestTest {

    private static final BigDecimal
            MINUS_TEN = BigDecimal.valueOf(-10);

    SearchRequest req = new SearchRequest();

    void warn() {
        assertTrue(req.warnThatCostFromIsMoreThanCostTo());
    }

    void dontWarn() {
        assertFalse(req.warnThatCostFromIsMoreThanCostTo());
    }

    //<editor-fold defaultstate="collapsed" desc="don't warn if has invalid costs (they should be already rejected with another message)">
    @Test public void fromNull_toValid() {
        req.costTo = TEN;
        dontWarn();
    }

    @Test public void toNull_fromValid() {
        req.costFrom = TEN;
        dontWarn();
    }

    @Test public void fromNull_toNegative() {
        req.costTo = MINUS_TEN;
        dontWarn();
    }

    @Test public void toNull_fromNegative() {
        req.costFrom = MINUS_TEN;
        dontWarn();
    }

    @Test public void bothNull() {
        dontWarn();
    }

    @Test public void bothNegative() {
        req.costFrom = MINUS_TEN;
        req.costTo   = MINUS_TEN;
        dontWarn();
    }

    @Test public void toValid_fromNegative() {
        req.costFrom = MINUS_TEN;
        req.costTo   = TEN;
        dontWarn();
    }

    @Test public void toNegative_fromValid() {
        req.costFrom = TEN;
        req.costTo   = MINUS_TEN;
        dontWarn();
    }
    //</editor-fold>

    @Test public void equals() {
        req.costFrom = TEN;
        req.costTo   = TEN;
        dontWarn();
    }

    @Test public void fromLess_0() {
        req.costFrom = ZERO;
        req.costTo   = TEN;
        dontWarn();
    }

    @Test public void fromLess_1() {
        req.costFrom = ONE;
        req.costTo   = TEN;
        dontWarn();
    }

    //<editor-fold defaultstate="collapsed" desc="warn">
    @Test public void fromMore_0() {
        req.costFrom = TEN;
        req.costTo   = ZERO;
        warn();
    }

    @Test public void fromMore_1() {
        req.costFrom = TEN;
        req.costTo   = ONE;
        warn();
    }
    //</editor-fold>
}