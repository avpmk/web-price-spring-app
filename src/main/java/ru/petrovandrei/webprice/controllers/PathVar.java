package ru.petrovandrei.webprice.controllers;

public class PathVar {

    public static final String
            ID               = "{id:[0-9]*}",
            PREFIX_ID        = "id" + ID,
            ID_DELETE        = ID + "/delete",
            PREFIX_ID_DELETE = PREFIX_ID + "/delete";

}