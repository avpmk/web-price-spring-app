package ru.petrovandrei.webprice.dao;

import java.util.HashSet;

public interface CategoriesRepositoryMixinDef {
    HashSet<Integer> categoryIdsAvailableForUser();
}