<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/resources/ui.dropdownchecklist.css" />
        <link rel="stylesheet" type="text/css" href="/resources/main.css" />
    </head>
    <body>

        <table>
            <c:forEach var="vendor" items="${vendors}">
                <tr>
                    <td><a href="/vendor${vendor.id}">${vendor.id}</a></td>
                    <td><a href="/vendor${vendor.id}">${vendor.name}</a></td>
                    <td><a href="/vendor${vendor.id}">${vendor.description}</a></td>
                </tr>
            </c:forEach>
        </table>
        <div class="hyp">
            <a href="/vendor/create">create vendor</a>
            <br/>
            <a href="/product/search">view products</a>
        </div>
    </body>
</html>