package ru.petrovandrei.webprice.dao;

import java.util.HashSet;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.petrovandrei.webprice.domain.Category;

public interface CategoriesRepository extends JpaRepository<Category, Integer>, CategoriesRepositoryMixinDef {

    @CacheEvict(value = "categoriesCache", allEntries = true)
    @Override Category save(Category category);

    @Cacheable(value = "categoriesCache", key = "'ids'")
    @Override HashSet<Integer> categoryIdsAvailableForUser();
}