package ru.petrovandrei.util;

import java.util.HashSet;
import lombok.val;

public class ArrayUtil {

    public static <T> T[] arrayOf(T... arr) {
        return arr;
    }

    public static <T> HashSet<T> arraysToHashSet(T[][] arrays) {
        int size = 0;
        for (val arr : arrays) {
            size += arr.length;
        }

        val set = new HashSet<T>(size);
        for (val arr : arrays) {
            for (val e : arr) {
                set.add(e);
            }
        }

        return set;
    }

    public static <T> HashSet<T> hashSetOf(T... array) {
        val set = new HashSet<T>(array.length);
        for (val e : array) {
            set.add(e);
        }
        return set;
    }
}