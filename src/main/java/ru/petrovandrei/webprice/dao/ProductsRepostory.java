package ru.petrovandrei.webprice.dao;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.petrovandrei.webprice.domain.Product;

public interface ProductsRepostory extends JpaRepository<Product, Integer> {

    @Query(
            "SELECT p FROM Product p " +
            "WHERE " +
            " (:noCategories IS TRUE OR p.category.id IN (:c)) AND " +
            " (:q IS NULL OR :q = '' OR LOWER(p.name) LIKE CONCAT(LOWER(:q), '%')) AND " +
            " (:f IS NULL OR p.price >= :f) AND " +
            " (:t IS NULL OR p.price <= :t)"
    )
    List<Product> findBy(
            @Param("noCategories") boolean noCategories,
            @Param("c") List<Integer> categoryIds,
            @Param("q") String nameStartsWith,
            @Param("f") BigDecimal costFrom,
            @Param("t") BigDecimal costTo
    );

    @Query(
            "SELECT p.image FROM Product p " +
            "WHERE p.id = :id"
    )
    byte[] findImageById(@Param("id") Integer id);
}