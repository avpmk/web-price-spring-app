package ru.petrovandrei.webprice.controllers;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.petrovandrei.webprice.domain.Category;
import ru.petrovandrei.webprice.domain.Product;
import ru.petrovandrei.webprice.domain.Vendor;
import ru.petrovandrei.webprice.services.CategoriesService;
import ru.petrovandrei.webprice.services.ProductsService;
import ru.petrovandrei.webprice.services.VendorsService;

import static ru.petrovandrei.util.NullEmptyCheckUtil.isNullOrEmpty;
import static ru.petrovandrei.util.mapper.MapperUtil.mapper;
import static ru.petrovandrei.util.math.BigUtil.isMore;
import static ru.petrovandrei.util.math.BigUtil.isNegative;
import static ru.petrovandrei.util.springmvc.ex.ResourceNotFoundException.checkIsDefined;
import static ru.petrovandrei.webprice.controllers.ExceptionHandleUtil.showExceptionToUser;
import static ru.petrovandrei.webprice.controllers.PathVar.ID;
import static ru.petrovandrei.webprice.controllers.PathVar.ID_DELETE;
import static ru.petrovandrei.webprice.controllers.PathVar.PREFIX_ID;
import static ru.petrovandrei.webprice.controllers.PathVar.PREFIX_ID_DELETE;
import static ru.petrovandrei.webprice.controllers.Routes.redirectToMainPage;
import static ru.petrovandrei.webprice.domain.BaseEntity.idAsTextOrNull;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

@Controller
@RequestMapping("/product")
public class ProductsController {

    @Inject
    CategoriesService categoriesService;

    @Inject
    VendorsService vendorsService;

    @Inject
    ProductsService productsService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        //todo switch to java 8 time api
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));

        binder.registerCustomEditor(Category.class, mapper(
                categoriesService::findByIdOrNull,
                idAsTextOrNull()
        ));

        binder.registerCustomEditor(Vendor.class, mapper(
                vendorsService::findByIdOrNull,
                idAsTextOrNull()
        ));
    }

    //<editor-fold defaultstate="collapsed" desc="create">
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(
            Map<String, Object> model,
            @ModelAttribute("product") Product product
    ) {
        markIsNew(model);
        addEditFormData(model);
        return "products/edit";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(
            Map<String, Object> model,
            @Valid
            @ModelAttribute("product") Product product,
            BindingResult br,
            @RequestParam(name = "imageFile", required = false) MultipartFile image
    ) {
        log.info("create product: {}", product);

        if (br.hasErrors()) {
            return create(model, product);
        } else {
            try {
                productsService.save(product, image);
            } catch (DataAccessException | IOException e) {
                showExceptionToUser(model, e);
                return create(model, product);
            }
            return redirectToMainPage();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="read">
    @RequestMapping(value = {ID, PREFIX_ID}, method = RequestMethod.GET)
    public String read(
            Map<String, Object> model,
            @PathVariable("id") Integer id
    ) {
        checkIsDefined(id);
        val product = productsService.findById(id);
        checkIsDefined(product);
        model.put("product", product);
        addEditFormData(model);
        return "products/edit";
    }

    @RequestMapping(value = {ID + "/mainImage", PREFIX_ID + "/mainImage"}, method = RequestMethod.GET)
    public void readImage(
            @PathVariable("id") int id, HttpServletResponse response
    ) throws IOException {
        byte[] image = productsService.findImageById(id);
        log.info(
                "productsService.findImageById, image size: {}",
                image == null ? "no image" : image.length
        );
        if (image != null) {
            try (OutputStream os = response.getOutputStream()) {
                os.write(image);
            }
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="search">
    @Data public static class SearchRequest {
        /** categoryIds */
        List<Integer> c;
        String text;

        @Min(value = 0, message = "Cost bound cannot be negative, sorry =)")
        BigDecimal costFrom, costTo;

        boolean warnThatCostFromIsMoreThanCostTo() {
            return validCost(costFrom) && validCost(costTo) && isMore(costFrom, costTo);
        }

        public boolean isEmpty() {
            return
                    isNullOrEmpty(c) &&
                    isNullOrEmpty(text) &&
                    costFrom == null && costTo == null;
        }

        private static boolean validCost(BigDecimal cost) {
            return cost != null && !isNegative(cost);
        }
    }

    @RequestMapping("/search")
    public String search(Map<String, Object> model, @Valid SearchRequest searchRequest, BindingResult br) {
        log.info("SearchController#search: {}", searchRequest);

        if (!searchRequest.isEmpty()) {
            //<editor-fold defaultstate="collapsed" desc="additional validation">
            //todo replace "hz"
            val hzCode = "hz";

            if (searchRequest.warnThatCostFromIsMoreThanCostTo()) {
                String msg = "'Cost from' connot be more than 'cost to'";
                //todo: make spring show message once (but highlight both field)
                br.rejectValue("costFrom", hzCode, msg);
                br.rejectValue("costTo", hzCode, msg);
            }

            if (!categoriesService.isAllCategoriesAvailableForCurrentUser(searchRequest.c)) {
                br.rejectValue("c", hzCode, "Requested not available category");
                model.put("hasRequestedNonAvailableCategories", true);
            }
            //</editor-fold>

            if (!br.hasErrors()) {
                model.put("foundProducts", productsService.findBy(searchRequest));
            }
        }

        addAvailableCategories(model);

        return "search";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="update">
    @RequestMapping(value = {ID, PREFIX_ID}, method = RequestMethod.POST)
    public String update(
            Map<String, Object> model,
            @PathVariable("id") int id,
            @Valid @ModelAttribute("product") Product product,
            BindingResult br,
            @RequestParam(name = "imageFile", required = false) MultipartFile image
    ) {
        log.info("create product: {}", product);

        if (br.hasErrors()) {
            addEditFormData(model);
            return "products/edit";
        } else {
            try {
                product.specifyId(id);
                productsService.save(product, image);
            } catch (DataAccessException | IOException e) {
                showExceptionToUser(model, e);
                addEditFormData(model);
                return "products/edit";
            }
            return redirectToMainPage();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="delete">
    @RequestMapping(value = {ID_DELETE, PREFIX_ID_DELETE}, method = RequestMethod.POST)
    public String delete(
            @PathVariable("id") Integer id
    ) {
        log.info("delete request, id: {}", id);
        if (id != null) {
            productsService.delete(id);
        }
        return redirectToMainPage();
    }
    //</editor-fold>

    private void addAvailableCategories(Map<String, Object> model) {
        model.put(
                "availableCategories",
                categoriesService.getCategoriesAvailableForCurrentUser()
        );
    }

    private void addEditFormData(Map<String, Object> model) {
        addAvailableCategories(model);
        model.put(
                "vendors",
                vendorsService.findAll()
        );
    }

    private static void markIsNew(Map<String, Object> model) {
        model.put("isNew", true);
    }
}