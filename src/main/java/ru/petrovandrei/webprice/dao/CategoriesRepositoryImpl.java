package ru.petrovandrei.webprice.dao;

import java.util.HashSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

public class CategoriesRepositoryImpl implements CategoriesRepositoryMixinDef {

    @PersistenceContext
    EntityManager em;

    @Override public HashSet<Integer> categoryIdsAvailableForUser() {
        log.info("hashset generation with available category Ids");
        return new HashSet<>(query(
                "SELECT c.id FROM Category c", Integer.class
        ));
    }

    private <T> List<T> query(String jpql, Class<T> cls) {
        return em.createQuery(jpql, cls).getResultList();
    }
}