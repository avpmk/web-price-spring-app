package ru.petrovandrei.webprice.helpers;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import lombok.val;
import org.springframework.transaction.annotation.Transactional;
import ru.petrovandrei.webprice.dao.CategoriesRepository;
import ru.petrovandrei.webprice.dao.ProductsRepostory;
import ru.petrovandrei.webprice.domain.Category;
import ru.petrovandrei.webprice.domain.Product;

import static ru.petrovandrei.webprice.domain.Category.newCategory;
import static ru.petrovandrei.webprice.domain.Product.p;

public class JpaSampleDataInflator {

    @Inject
    CategoriesRepository cr;

    @Inject
    ProductsRepostory pr;

    @PostConstruct
    void inflate() {

        //<editor-fold defaultstate="collapsed" desc="food">
        add(
                "Продукты питания",
                p("Хлеб", 30), p("Батон", 35), p("Печенье", 200),
                p("Молоко", 50), p("Сметана", 60), p("Творог", 150),
                p("Йогурт", 70), p("Ряженка", 65), p("Варенец", 77),
                p("Свинина", 450), p("Курица", 160), p("Говядина", 450),
                p("Сахар", 60),
                //рыба
                p("Кета", 400), p("Сёмга", 800), p("Форель", 990),
                p("Скумбрия", 300), p("Горбуша", 270),
                p("Мука", 60),
                p("Макароны", 30),
                p("Рис", 60), p("Гречка", 89), p("Пшено", 30),
                p("Фасоль", 60), p("Горох", 45),
                p("Чай черный листовой", 150), p("Чай зеленый листовой", 145),
                p("Чай черный в пакетиках", 69), p("Чай зеленый в пакетиках", 68),
                p("Шоколад Молочный", 82), p("Шоколад Темный", 90), p("Шоколад Горький", 200),
                p("Конфеты шоколадные", 300), p("Конфеты карамель", 230),
                p("Конфеты Батончик", 230),
                p("Батончик - конфеты", 230)
        );
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="wear">
        add(
                "Одежда",
                p("Джинсы", 3400),
                p("Рубашка", 1000),
                p("Носки", 40),
                p("Галстук", 410),
                p("Водолазка", 300),
                p("Пиджак мужской", 10000),
                p("Пуловер", 400),
                p("Брюки", 3000),
                p("Капри", 2800),
                p("Бриджи", 2400),
                p("Колготы", 400),
                p("Юбка", 2000),
                p("Платье", 5000),
                p("Пиджак женский", 12000),
                p("Жилет", 3400)
        );
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="child">
        add(
                "Детское",
                p("Пеленка байковая", 100),
                p("Пеленка", 80),
                p("Маечка", 55),
                p("Ползунки", 100),
                p("Штанишки", 250),
                p("Шапочка", 230),
                p("Чепчик", 45),
                p("Пинетки", 20),
                p("Рукавички", 28),
                p("Распашонка", 69),
                p("Подгузники 100", 1500),
                p("Футболка", 110)
        );
        //</editor-fold>

    }

    @Transactional
    void add(String categoryName, Product... productsInThisCategory) {
        val category = cr.save(newCategory(categoryName));
        for (val product : productsInThisCategory) {
            product.setCategory(category);
            pr.save(product);
        }
        cr.flush();
        pr.flush();
    }
}