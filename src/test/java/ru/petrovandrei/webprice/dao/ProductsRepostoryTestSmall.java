package ru.petrovandrei.webprice.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.petrovandrei.webprice.domain.Category;
import ru.petrovandrei.webprice.domain.Product;

import static ru.petrovandrei.webprice.domain.Product.p;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
    "classpath:spring-datasource-test-config.xml",
    "classpath:spring-persistence-config.xml"
})
public class ProductsRepostoryTestSmall extends ProductsRepostoryTestHelpersApi {

    Product
            tshort = p("Майка", 500),
            jeans  = p("Джинсы", 1000),
            pizza  = p("Pizza", 500),
            sushi  = p("Sushi", 1000);

    Product[]
            wear = {tshort, jeans},
            food = {pizza, sushi};

    Category
            categoryWear,
            categoryFood;

    @Override void inflate() {
        categoryWear = add("Одежда", wear);
        categoryFood = add("Еда", food);
    }

    @Test public void shouldFindAll() {
        cats(categoryWear, categoryFood);
        search();
        shouldBeFound_ignoreOrder(wear, food);
    }

    @Test public void shouldFindMoreThan700() {
        from = 700;
        search();
        shouldBeFound_ignoreOrder(
                jeans, sushi
        );
    }

    @Test public void shouldFindLessThan700() {
        to = 700;
        search();
        shouldBeFound_ignoreOrder(
                tshort, pizza
        );
    }
}