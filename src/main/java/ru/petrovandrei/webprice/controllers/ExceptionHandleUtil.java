package ru.petrovandrei.webprice.controllers;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

public class ExceptionHandleUtil {

    public static void showExceptionToUser(Map<String, Object> model, Exception ex) {
        model.put("errorMessage", "Data is not saved. Try again later. Reason: " + ex);
        log.error("Data is not saved. Exception: ", ex);
    }
}