package ru.petrovandrei.webprice.controllers;

import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.petrovandrei.webprice.domain.Vendor;
import ru.petrovandrei.webprice.services.VendorsService;

import static ru.petrovandrei.util.springmvc.ex.ResourceNotFoundException.checkIsDefined;
import static ru.petrovandrei.webprice.controllers.ExceptionHandleUtil.showExceptionToUser;
import static ru.petrovandrei.webprice.controllers.PathVar.ID;
import static ru.petrovandrei.webprice.controllers.PathVar.ID_DELETE;
import static ru.petrovandrei.webprice.controllers.Routes.redirectToVendors;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

@Controller
public class VendorsController {

    private static final String VENDOR = "vendor";

    @Inject
    VendorsService vendorsService;

    //<editor-fold defaultstate="collapsed" desc="create">
    @RequestMapping(value = VENDOR + "/create", method = RequestMethod.GET)
    public String create(@ModelAttribute("vendor") Vendor vendor) {
        return "vendors/edit";
    }

    @RequestMapping(value = VENDOR + "/create", method = RequestMethod.POST)
    public String create(
            Map<String, Object> model,
            @Valid
            @ModelAttribute("vendor") Vendor vendor,
            BindingResult br
    ) {
        log.info("create vendor: {}", vendor);

        if (br.hasErrors()) {
            return create(vendor);
        } else {
            try {
                vendorsService.save(vendor);
            } catch (DataAccessException e) {
                showExceptionToUser(model, e);
                return create(vendor);
            }
            return redirectToVendors();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="read">
    @RequestMapping(value = VENDOR + ID, method = RequestMethod.GET)
    public String read(Model model, @PathVariable("id") Integer id) {
        checkIsDefined(id);
        val vendor = vendorsService.findById(id);
        checkIsDefined(vendor);
        model.addAttribute(vendor);
        return "vendors/edit";
    }

    @RequestMapping(value = "vendors", method = RequestMethod.GET)
    public String readAll(Map<String, Object> model) {
        model.put("vendors", vendorsService.findAll());
        return "vendors/all";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="update">
    @RequestMapping(value = VENDOR + ID, method = RequestMethod.POST)
    public String update(
            Map<String, Object> model,
            @PathVariable("id") int id,
            @Valid @ModelAttribute("vendor") Vendor vendor,
            BindingResult br
    ) {
        log.info("create vendor: {}", vendor);

        if (br.hasErrors()) {
            return "vendors/edit";
        } else {
            try {
                vendor.specifyId(id);
                vendorsService.save(vendor);
            } catch (DataAccessException e) {
                showExceptionToUser(model, e);
                return "vendors/edit";
            }
            return redirectToVendors();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="delete">
    @RequestMapping(value = VENDOR + ID_DELETE, method = RequestMethod.POST)
    public String delete(
            Map<String, Object> model,
            @PathVariable("id") Integer id
    ) {
        log.info("delete vendor, id: {}", id);
        if (id != null) {
            try {
                vendorsService.delete(id);
            } catch (DataAccessException e) {
                showExceptionToUser(model, e);
                model.put("vendor", vendorsService.findById(id));
                return "vendors/edit";
            }
        }
        return redirectToVendors();
    }
    //</editor-fold>

}