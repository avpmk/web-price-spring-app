package ru.petrovandrei.util.springmvc.ex;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

    public static void checkIsDefined(Object id) throws ResourceNotFoundException {
        if (id == null) throw new ResourceNotFoundException();
    }
}