package ru.petrovandrei.webprice.dao;

import javax.inject.Inject;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
    "classpath:spring-datasource-test-config.xml",
    "classpath:spring-persistence-config.xml"
})
public class CategoriesRepositoryTestWithoutCache {

    @Inject
    CategoriesRepository cr;

    @Test public void shouldNotBeSameReference() {
        val a = cr.categoryIdsAvailableForUser();
        val b = cr.categoryIdsAvailableForUser();
        assertTrue(a != b);
    }
}