package ru.petrovandrei.webprice.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static ru.petrovandrei.webprice.controllers.Routes.redirectToMainPage;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

@Controller
public class HomeController {

    @RequestMapping("/")
    public String main() {
        log.info("request open home page");
        //todo for authenticated users make redirect to their prefered url
        return redirectToMainPage();
    }
}