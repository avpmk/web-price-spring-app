package ru.petrovandrei.util.math;

import java.math.BigDecimal;
import org.junit.Test;

import static org.junit.Assert.*;

public class BigUtilTest {

    //<editor-fold defaultstate="collapsed" desc="less with numbers">
    @Test public void _1_less_2() {
        assertTrue(less(1, 2));
    }

    @Test public void _1000_less_2000() {
        assertTrue(less(1000, 2000));
    }

    @Test public void _0_less_MaxLong() {
        assertTrue(less(0, Long.MAX_VALUE));
    }

    @Test public void _1000_less_MaxLong() {
        assertTrue(less(1000, Long.MAX_VALUE));
    }

    @Test public void _minus_1000_less_MaxLong() {
        assertTrue(less(-1000, Long.MAX_VALUE));
    }

    @Test public void _MinLong_less_MaxLong() {
        assertTrue(less(Long.MIN_VALUE, Long.MAX_VALUE));
    }

    @Test public void _0_less_1() {
        assertTrue(less(0, 1));
    }

    @Test public void _minus_1_less_0() {
        assertTrue(less(-1, 0));
    }

    @Test public void _minus_1000_less_minus100() {
        assertTrue(less(-1000, -100));
    }
    //</editor-fold>

    static boolean less(long a, long b) {
        return BigUtil.isLess(b(a), b(b));
    }

    //<editor-fold defaultstate="collapsed" desc="more with numbers">
    @Test public void _2_more_1() {
        assertTrue(more(2, 1));
    }

    @Test public void _200_more_100() {
        assertTrue(more(200, 100));
    }

    @Test public void _1_more_0() {
        assertTrue(more(1, 0));
    }
    //</editor-fold>

    static boolean more(long a, long b) {
        return BigUtil.isMore(b(a), b(b));
    }

    static BigDecimal b(long value) {
        return BigDecimal.valueOf(value);
    }
}