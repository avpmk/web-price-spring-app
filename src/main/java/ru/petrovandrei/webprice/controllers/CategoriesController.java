package ru.petrovandrei.webprice.controllers;

import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.petrovandrei.webprice.domain.Category;
import ru.petrovandrei.webprice.services.CategoriesService;

import static ru.petrovandrei.util.springmvc.ex.ResourceNotFoundException.checkIsDefined;
import static ru.petrovandrei.webprice.controllers.ExceptionHandleUtil.showExceptionToUser;
import static ru.petrovandrei.webprice.controllers.PathVar.ID;
import static ru.petrovandrei.webprice.controllers.PathVar.ID_DELETE;
import static ru.petrovandrei.webprice.controllers.Routes.redirectToCategories;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

@Controller
public class CategoriesController {

    private static final String CATEGORY = "category";

    @Inject
    CategoriesService categoriesService;

    //<editor-fold defaultstate="collapsed" desc="create">
    @RequestMapping(value = CATEGORY + "/create", method = RequestMethod.GET)
    public String create(@ModelAttribute("category") Category category) {
        return "categories/edit";
    }

    @RequestMapping(value = CATEGORY + "/create", method = RequestMethod.POST)
    public String create(
            Map<String, Object> model,
            @Valid
            @ModelAttribute("category") Category category,
            BindingResult br
    ) {
        log.info("create category: {}", category);

        if (br.hasErrors()) {
            return create(category);
        } else {
            try {
                categoriesService.save(category);
            } catch (DataAccessException e) {
                showExceptionToUser(model, e);
                return create(category);
            }
            return redirectToCategories();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="read">
    @RequestMapping(value = CATEGORY + ID, method = RequestMethod.GET)
    public String read(Model model, @PathVariable("id") Integer id) {
        checkIsDefined(id);
        val category = categoriesService.findById(id);
        checkIsDefined(category);
        model.addAttribute(category);
        return "categories/edit";
    }

    @RequestMapping(value = "categories", method = RequestMethod.GET)
    public String readAll(Map<String, Object> model) {
        model.put("categories", categoriesService.findAll());
        return "categories/all";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="update">
    @RequestMapping(value = CATEGORY + ID, method = RequestMethod.POST)
    public String update(
            Map<String, Object> model,
            @PathVariable("id") int id,
            @Valid @ModelAttribute("category") Category category,
            BindingResult br
    ) {
        log.info("create category: {}", category);

        if (br.hasErrors()) {
            return "categories/edit";
        } else {
            try {
                category.specifyId(id);
                categoriesService.save(category);
            } catch (DataAccessException e) {
                showExceptionToUser(model, e);
                return "categories/edit";
            }
            return redirectToCategories();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="delete">
    @RequestMapping(value = CATEGORY + ID_DELETE, method = RequestMethod.POST)
    public String delete(
            Map<String, Object> model,
            @PathVariable("id") Integer id
    ) {
        log.info("delete category, id: {}", id);
        if (id != null) {
            try {
                categoriesService.delete(id);
            } catch (DataAccessException e) {
                showExceptionToUser(model, e);
                model.put("category", categoriesService.findById(id));
                return "categories/edit";
            }
        }
        return redirectToCategories();
    }
    //</editor-fold>

}