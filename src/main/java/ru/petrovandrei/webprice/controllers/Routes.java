package ru.petrovandrei.webprice.controllers;

public class Routes {

    public static final String
            PRODUCTS   = "/product/search",
            VENDORS    = "/vendors",
            CATEGORIES = "/categories";

    public static String redirectToMainPage() {
        return "redirect:" + PRODUCTS;
    }

    public static String redirectToVendors() {
        return "redirect:" + VENDORS;
    }

    public static String redirectToCategories() {
        return "redirect:" + CATEGORIES;
    }
}