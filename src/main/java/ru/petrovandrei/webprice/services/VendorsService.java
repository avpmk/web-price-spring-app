package ru.petrovandrei.webprice.services;

import java.util.List;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.petrovandrei.webprice.dao.VendorsRepository;
import ru.petrovandrei.webprice.domain.Vendor;

import static java.lang.Integer.parseInt;
import static ru.petrovandrei.util.NullEmptyCheckUtil.isNullOrEmpty;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

@Service
public class VendorsService {

    @Inject
    VendorsRepository vendorsRepostory;

    public Vendor findByIdOrNull(String textIntId) {
        int id = 0;
        if (!isNullOrEmpty(textIntId)) {
            id = parseInt(textIntId);
        }

        return id > 0 ? vendorsRepostory.findOne(id) : null;
    }

    public List<Vendor> findAll() {
        log.info("VendorsService#findAll");
        return vendorsRepostory.findAll();
    }

    public Vendor findById(Integer id) {
        log.info("VendorsService#findById");
        return vendorsRepostory.findOne(id);
    }

    public void save(Vendor vendor) {
        vendorsRepostory.save(vendor);
    }

    public void delete(Integer id) {
        vendorsRepostory.delete(id);
    }
}