package ru.petrovandrei.webprice.dao;

import java.util.Collections;
import java.util.stream.LongStream;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.petrovandrei.webprice.domain.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
    "classpath:spring-datasource-test-config.xml",
    "classpath:spring-persistence-config.xml"
})
public class ProductsRepostoryTest extends ProductsRepostoryTestHelpersApi {

    @Override void inflate() {
    }

    //service will not search if there is no params
    @Test public void findAllIfThereIsNoRestrictions() {
        val savedList = add(
            LongStream.range(42, 43).mapToObj(num -> new Product("prod " + num, num))
        );
        search();
        Collections.shuffle(savedList);
        shouldBeFound_ignoreOrder(savedList);
    }
}