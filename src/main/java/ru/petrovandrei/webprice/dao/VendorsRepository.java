package ru.petrovandrei.webprice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.petrovandrei.webprice.domain.Vendor;

public interface VendorsRepository extends JpaRepository<Vendor, Integer> {

}