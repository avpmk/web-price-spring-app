package ru.petrovandrei.util.math;

import java.math.BigDecimal;

public class BigUtil {

    //todo add tests
    public static boolean isLess(BigDecimal that, BigDecimal than) {
        return that.compareTo(than) < 0;
    }

    public static boolean isNegative(BigDecimal that) {
        return isLess(that, BigDecimal.ZERO);
    }

    //todo add tests
    public static boolean isMore(BigDecimal that, BigDecimal than) {
        return that.compareTo(than) > 0;
    }
}