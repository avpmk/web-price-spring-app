package ru.petrovandrei.webprice.controllers;

import java.util.HashMap;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindingResult;
import ru.petrovandrei.webprice.controllers.ProductsController.SearchRequest;
import ru.petrovandrei.webprice.domain.Product;
import ru.petrovandrei.webprice.services.CategoriesService;
import ru.petrovandrei.webprice.services.ProductsService;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductsControllerTest {

    ProductsController controller = new ProductsController();
    HashMap<String, Object> model = new HashMap<>();

    SearchRequest searchRequest = mock(SearchRequest.class);
    BindingResult br = mock(BindingResult.class);

    CategoriesService cs = mock(CategoriesService.class);
    ProductsService ps = mock(ProductsService.class);

    @Before public void injectServices() {
        controller.categoriesService = cs;
        controller.productsService = ps;
    }

    //<editor-fold defaultstate="collapsed" desc="initializing api">
    private void setBrHasErrors() {
        when(br.hasErrors()).thenReturn(true);
    }

    private void setBrHasNotErrors() {
        when(br.hasErrors()).thenReturn(false);
    }


    private void setCategoriesIsAvailable() {
        when(cs.isAllCategoriesAvailableForCurrentUser(any())).thenReturn(true);
    }

    private void setCategoriesIsNotAvailable() {
        when(cs.isAllCategoriesAvailableForCurrentUser(any())).thenReturn(false);
    }


    private void setSearchRequestIsEmpty() {
        when(searchRequest.isEmpty()).thenReturn(true);
    }
    //</editor-fold>

    @After public void modelShouldHaveAvailableCategories_afterSearchMethodCall() {
        assertNotNull(model.get("availableCategories"));
    }

    @Test public void dontSearchIfSearchRequestIsEmpty() {
        setSearchRequestIsEmpty();
        modelShouldNotHaveFoundProducts();
    }

    @Test public void dontSearchIfBindingResultHasError() {
        assertFalse(searchRequest.isEmpty());
        setBrHasErrors();
        modelShouldNotHaveFoundProducts();
    }

    @Test public void shouldAddToModel_hasRequestedNonAvailableCategories_ifSo() {
        assertFalse(searchRequest.isEmpty());
        setBrHasErrors();
        setCategoriesIsNotAvailable(); //in fact now it is already doing like that
        modelShouldNotHaveFoundProducts();
        assertTrue((boolean) model.get("hasRequestedNonAvailableCategories"));
    }

    @Test public void shouldNotAddToModel_hasRequestedNonAvailableCategories_ifIsnt() {
        assertFalse(searchRequest.isEmpty());
        setBrHasErrors();
        setCategoriesIsAvailable();
        modelShouldNotHaveFoundProducts();
        assertNull(model.get("hasRequestedNonAvailableCategories"));
    }

    @Test public void searchIfBindingResultHasNotError() {
        setBrHasNotErrors();
        val resultList = asList(
                new Product(), new Product(), new Product()
        );

        when(ps.findBy(any())).thenReturn(resultList);
        search();

        assertEquals(resultList, model.get("foundProducts"));
    }

    void search() {
        controller.search(model, searchRequest, br);
    }

    void modelShouldNotHaveFoundProducts() {
        search();
        assertNull(model.get("foundProducts"));
    }
}