package ru.petrovandrei.webprice.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.petrovandrei.util.springmvc.ex.ResourceNotFoundException;

//<editor-fold defaultstate="collapsed" desc="log">
@Slf4j
//</editor-fold>

@ControllerAdvice
public class ExceptionControllerAdvice {

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(Throwable.class)
    public String genericExceptionHandler(Exception ex) {
        log.error("catched in ExceptionControllerAdvice", ex);
        return "error";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    public String handleResourceNotFoundException() {
        return "404";
    }
}