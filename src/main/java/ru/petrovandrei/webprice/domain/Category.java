package ru.petrovandrei.webprice.domain;

import javax.persistence.Entity;
import lombok.NoArgsConstructor;

//<editor-fold defaultstate="collapsed" desc="boiler-plate generation">
@NoArgsConstructor
//</editor-fold>

@Entity
public class Category extends BaseEntity {

    //<editor-fold defaultstate="collapsed" desc="constructor">
    public Category(String name, String description) {
        super(name, description);
    }
    //</editor-fold>

    @Override protected final boolean instanceOfProperClass(BaseEntity another) {
        return another instanceof Category;
    }

    public static Category newCategory(String name) {
        Category c = new Category();
        c.setName(name);
        return c;
    }
}