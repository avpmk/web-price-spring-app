<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/resources/ui.dropdownchecklist.css" />
        <link rel="stylesheet" type="text/css" href="/resources/main.css" />
    </head>
    <body>

        <sf:form method="post" modelAttribute="product" enctype="multipart/form-data">
            <c:if test="${!isNew}">
                <img src="/product/id${product.id}/mainImage" />
                <!--onerror="/resources/image"-->
            </c:if>

            <br/>

            <fieldset class="inpedit">
                <label>Name:</label>
                <sf:input path="name" cssErrorClass="error-field" />
                <br/>
                <sf:errors path="name" cssClass="error-message" />
                <br/>
                <label>Description:</label>
                <sf:input  path="description" />
                <br/>
                <sf:errors path="description" cssClass="error-message" />
                <br/>
                <label>Vendor:</label>
                <sf:select path="vendor" items="${vendors}" itemLabel="name" itemValue="id" />
                <br/>
                <a href="/vendors">All vendors</a>
                <br/>
                <label>Price:</label>
                <sf:input  path="price" cssErrorClass="error-field" />
                <br/>
                <sf:errors path="price" cssClass="error-message" />
                <br/>
                <label>DateAddition:</label>
                <sf:input  path="dateAddition" type="date" cssErrorClass="error-field"/>
                <br/>
                <sf:errors path="dateAddition" cssClass="error-message" />
                <br/>
                <label>Category:</label>
                <sf:select path="category" items="${availableCategories}" itemLabel="name" itemValue="id" />
                <br/>
                <a href="/categories">All categories</a>
                <br/>                     
                <input name="imageFile" type="file"/>
                <br/>
                <sf:errors path="*" cssClass="error-message" element="div" delimiter="<br/>" />
                <input type="submit" value="send" />
            </fieldset>
        </sf:form>

        <c:if test="${!isNew}">
            <form method="post" action="/product/id${product.id}/delete" class="del">
                <input type="submit" value="delete" />
            </form>
        </c:if>

        <c:if test="${errorMessage != null}">
            <div class="error-box">
                ${errorMessage}
            </div>
        </c:if>

    </body>
</html>