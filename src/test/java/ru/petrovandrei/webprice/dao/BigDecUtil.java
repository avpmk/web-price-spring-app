package ru.petrovandrei.webprice.dao;

import java.math.BigDecimal;

public class BigDecUtil {

    public static BigDecimal toBigDec(Number that) {
        if (that == null || that instanceof BigDecimal) {
            return (BigDecimal) that;
        }
        if (that instanceof Integer) {
            return new BigDecimal((int) that);
        }
        if (that instanceof Long) {
            return BigDecimal.valueOf((long)that);
        }
        if (that instanceof Double) {
            return BigDecimal.valueOf((double)that);
        }
        throw new UnsupportedOperationException(
                "Class " + that.getClass() + " is not supported."
        );
    }
}