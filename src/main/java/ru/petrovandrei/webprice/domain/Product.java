package ru.petrovandrei.webprice.domain;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//<editor-fold defaultstate="collapsed" desc="boiler-plate generation">
@NoArgsConstructor
@Getter @Setter
@ToString(callSuper = true, exclude = "image")
//</editor-fold>

@Entity
public class Product extends BaseEntity {

    @ManyToOne
    private Vendor vendor;

    @Min(value = 0, message = "Negative сost is not allowed")
    private BigDecimal price;

    @Temporal(TemporalType.DATE)
    private Date dateAddition;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    private byte[] image;

    @ManyToOne
    private Category category;

    public Product(String name, long price) {
        super(name);
        this.price = BigDecimal.valueOf(price);
    }

    public static Product p(String name, long price) {
        return new Product(name, price);
    }

    @Override protected final boolean instanceOfProperClass(BaseEntity another) {
        return another instanceof Product;
    }
}