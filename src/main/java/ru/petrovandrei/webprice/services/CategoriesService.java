package ru.petrovandrei.webprice.services;

import java.util.List;
import javax.inject.Inject;
import lombok.val;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.petrovandrei.webprice.dao.CategoriesRepository;
import ru.petrovandrei.webprice.domain.Category;

import static java.lang.Integer.parseInt;
import static ru.petrovandrei.util.NullEmptyCheckUtil.isNullOrEmpty;

@Service
public class CategoriesService {

    @Inject
    CategoriesRepository categoriesRepository;

    //<editor-fold defaultstate="collapsed" desc="must be changed together">
    public boolean isCategoryAvailableForCurrentUser(Category category) {
        if (category == null) {
            return true;
        }
        val availableIds = categoriesRepository.categoryIdsAvailableForUser(/*todo: current user*/);
        return availableIds.contains(category.getId());
    }

    public boolean isAllCategoriesAvailableForCurrentUser(List<Integer> categoryIds) {
        if (isNullOrEmpty(categoryIds)) {
            return true;
        }

        val availableIds = categoriesRepository.categoryIdsAvailableForUser(/*todo: current user*/);
        return categoryIds.stream().allMatch(availableIds::contains);
    }

    //todo move to repository
    @Cacheable(value = "categoriesCache", key = "'objects'")
    public List<Category> getCategoriesAvailableForCurrentUser() {
        //liberalistic again
        return categoriesRepository.findAll();
    }
    //</editor-fold>

    public Category findByIdOrNull(String textIntId) {
        int id = 0;
        if (!isNullOrEmpty(textIntId)) {
            id = parseInt(textIntId);
        }

        return id > 0 ? categoriesRepository.findOne(id) : null;
    }

    public Category findById(Integer id) {
        return categoriesRepository.findOne(id);
    }

    public List<Category> findAll() {
        return categoriesRepository.findAll();
    }

    public void save(Category category) {
        categoriesRepository.save(category);
    }

    public void delete(Integer id) {
        categoriesRepository.delete(id);
    }
}