<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/resources/ui.dropdownchecklist.css" />
        <link rel="stylesheet" type="text/css" href="/resources/main.css" />
        <c:if test="${hasRequestedNonAvailableCategories}">
            <style>
                .ui-dropdownchecklist {
                    border: 1px solid rgba(255, 0, 0, 0.5) !important;
                    box-shadow:  0 0 3px 0px rgba(255, 0, 0, 0.8) !important;
                }
                .ui-dropdownchecklist:hover {
                    border: 1px solid rgb(255, 0, 0) !important;
                    box-shadow:  0 0 3px 0px rgba(255, 0, 0, 1) !important;
                }
            </style>
        </c:if>
        <script type="text/javascript" src="/resources/jquery.js"></script>
        <script type="text/javascript" src="/resources/ui.core.js"></script>
        <script type="text/javascript" src="/resources/ui.dropdownchecklist.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#selectCategories").dropdownchecklist({defaultText: 'Categories...'});
            });
        </script>
    </head>
    <body>
        <sf:form method="get" modelAttribute="searchRequest">
            <sf:select id="selectCategories" multiple="true" path="c" items="${availableCategories}" itemLabel="name" itemValue="id" />
            <sf:input path="text" />
            <sf:input path="costFrom" cssErrorClass="error-field" />
            <sf:input path="costTo"   cssErrorClass="error-field" />
            <input type="submit" value="find!" />
            <br/>
            <sf:errors path="*" cssClass="error-message" element="div" delimiter="<br/>" />
        </sf:form>
        <c:choose>
            <c:when test="${foundProducts != null}">

                <c:choose>
                    <c:when test="${!foundProducts.isEmpty()}">
                        <h2>found products:</h2>
                        <div class="tbl">
                            <table class="products">
                                <c:forEach var="product" items="${foundProducts}">
                                    <tr>
                                        <td><a href="/product/id${product.id}">${product.id}</a></td>
                                        <td><a href="/product/id${product.id}">${product.name}</a></td>
                                        <td>${product.price}</td>
                                        <td><a href="/category${product.category.id}">${product.category.name}</a></td>
                                        <td><a href="/vendor${product.vendor.id}">${product.vendor.name}</a></td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <h2>Has not found anything matching the Request</h2>
                    </c:otherwise>
                </c:choose>

            </c:when>
            <c:when test="${searchRequest.isEmpty()}">
                <h2>The request must include at least one parameter.</h2>
            </c:when>
        </c:choose>

        <a class="hyp" href="/product/create">create product</a>

    </body>
</html>