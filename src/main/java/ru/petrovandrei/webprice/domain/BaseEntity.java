package ru.petrovandrei.webprice.domain;

import java.util.function.Function;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@MappedSuperclass
@NoArgsConstructor
@ToString
public abstract class BaseEntity {

    private static final int
            NAME_COLUMN_SIZE        = 100,
            DESCRIPTION_COLUMN_SIZE = 300;

    private static final String
            NAME_CONSTRAINT_MESSAGE        = "Name length must be in range of 1 to " + NAME_COLUMN_SIZE,
            DESCRIPTION_CONSTRAINT_MESSAGE = "Description cannot be longer than " + DESCRIPTION_COLUMN_SIZE;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private int id;

    @Deprecated
    public void specifyId(int id) {
        this.id = id;
    }

    @Getter @Setter
    @NotNull(message = NAME_CONSTRAINT_MESSAGE)
    @Size(min = 1, max = NAME_COLUMN_SIZE, message = NAME_CONSTRAINT_MESSAGE)
    @Column(length = NAME_COLUMN_SIZE)
    private String name;

    @Getter @Setter
    @Size(max = DESCRIPTION_COLUMN_SIZE, message = DESCRIPTION_CONSTRAINT_MESSAGE)
    @Column(length = DESCRIPTION_COLUMN_SIZE)
    private String description;

    //<editor-fold defaultstate="collapsed" desc="equals, hashCode">
    protected abstract boolean instanceOfProperClass(BaseEntity another);

    @Override public final boolean equals(Object another) {
        return another instanceof BaseEntity &&
                ((BaseEntity) another).id == id &&
                instanceOfProperClass((BaseEntity) another);
    }

    @Override public final int hashCode() {
        return id;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="constructors">
    public BaseEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public BaseEntity(String name) {
        this.name = name;
    }
    //</editor-fold>

    public static Function<BaseEntity, String> idAsTextOrNull() {
        return entity -> entity == null ? null : Integer.toString(entity.getId());
    }
}