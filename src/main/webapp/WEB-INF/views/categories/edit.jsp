<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/resources/ui.dropdownchecklist.css" />
        <link rel="stylesheet" type="text/css" href="/resources/main.css" />
    </head>
    <body>

        <sf:form method="post" modelAttribute="category" >
            <fieldset class="inpedit">
                <label>Name:</label>
                <sf:input  path="name" cssErrorClass="error-field" />
                <br/>
                <sf:errors path="name" cssClass="error-message" />
                <br/>
                <label>Description:</label>
                <sf:input  path="description" />
                <br/>
                <sf:errors path="description" cssClass="error-message" />
                <br/>
                <input type="submit" value="send" />
            </fieldset>
        </sf:form>

        <c:if test="${category.id != 0}">
            <form method="post" action="/category${category.id}/delete" class="del">
                <input type="submit" value="delete" />
            </form>
        </c:if>

        <c:if test="${errorMessage != null}">
            <div class="error-box">
                ${errorMessage}
            </div>
        </c:if>
        <div class="hyp">
            <a href="/categories">view categories</a>
            <br/>
            <a href="/product/search">view products</a>
        </div>
    </body>
</html>